$(function() {



// Смена картинок на главном экране

$(window).on('load', function(){

if ($(window).width() >= 1200) {
 var mainSrc = $('.banner').attr("data-background-image");
$( ".banner" ).css({ 'background-image' : 'url(' + mainSrc + ')' })

$( ".banner-item" ).hover(function(){ // задаем функцию при наведении курсора на элемент 
  var imgSrc =  $(this).attr("data-background-image");
 
  
  $(this).parent('.banner').css({ 'background-image' : 'url(' + imgSrc + ')' })

  }, function(){ // задаем функцию, которая срабатывает, когда указатель выходит из элемента  
  $(this).parent('.banner').css({ 'background-image' : 'url(' + mainSrc + ')' })
});

}

});




// brands-slider
  $('.brands-slider').slick({
      infinite: true,
      slidesToShow: 9,
      slidesToScroll: 1,
      // autoplay: true,
      // autoplaySpeed: 4000,
      arrows: true,
      dots: false,
      // variableWidth: true,

      responsive: [
      {
        breakpoint: 1600,
        settings: {
            slidesToShow: 7,
        }
        
      },
      {
        breakpoint: 1360,
        settings: {
            slidesToShow: 5,
        }
        
      },
      {
        breakpoint: 1199,
        settings: {
            slidesToShow: 4,
        }
        
      },
      {
        breakpoint: 991,
        settings: {
            slidesToShow: 3,
        }
        
      },
      {
        breakpoint: 479,
        settings: {
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            centerMode: true,
            variableWidth: true,
            arrows: false,
        }
        
      },
    ]
     
  });




// Меню в сайдбаре
$('.sidebar .drop > a, .modal-filters .drop > a').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('active');
  $(this).next('ul').slideToggle();
})

// Фильтры в сайдбаре
$('.sidebar-item__header--toggler').click(function() {
  $(this).toggleClass('active');
  $(this).next('.sidebar-item__body').slideToggle();
})

$('.sidebar-item__block--toggler').click(function() {

  $(this).next('.sidebar-item__block--body').slideToggle();
})




// Показать\скрыть чекбоксы в сайдбаре

$('.check-list__more').click(function() {
  
  $(this).parent().toggleClass('active');
  
  $(this).children('span').innerHTML = 
  ( $(this).text() === 'Показать все') ?  $(this).text('Скрыть')  :  $(this).text('Показать все');
})



// Показать\скрыть блоки фильтров в сайдбаре

$('.sidebar-item__block--more').click(function() {
  
  $(this).parent().toggleClass('active');
  
  $(this).children('span').innerHTML = 
  ( $(this).text() === 'Показать все фильтры') ?  $(this).text('Скрыть')  :  $(this).text('Показать все фильтры');
})


// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})



// Показать\скрыть характеристики
$('.compare-characteristics__item .h4').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('active');
  $(this).next('.compare-characteristics__item--wrap').toggleClass('active');
})



// Прикрепляем файлы
$('.attach').each(function() { // на случай, если таких групп файлов будет больше одной
  var attach = $(this),
    fieldClass = 'attach__item', // класс поля
    attachedClass = 'attach__item--attached', // класс поля с файлом
    fields = attach.find('.' + fieldClass).length, // начальное кол-во полей
    fieldsAttached = 0; // начальное кол-во полей с файлами

  var newItem = '<div class="attach__item"><label><div class="attach__up">Добавить файл</div><input class="attach__input" type="file" name="files[]" /></label><div class="attach__delete">&#10006;</div><div class="attach__name"></div><div class="attach__edit">Изменить</div></div>'; // разметка нового поля

  // При изменении инпута
  attach.on('change', '.attach__input', function(e) {
    var item = $(this).closest('.' + fieldClass),
      fileName = '';
    if (e.target.value) { // если value инпута не пустое
      fileName = e.target.value.split('\\').pop(); // оставляем только имя файла и записываем в переменную
    }
    if (fileName) { // если имя файла не пустое
      item.find('.attach__name').text(fileName); // подставляем в поле имя файла
      if (!item.hasClass(attachedClass)) { // если в поле до этого не было файла
        item.addClass(attachedClass); // отмечаем поле классом
        fieldsAttached++;
      }
      if (fields < 10 && fields == fieldsAttached) { // если полей меньше 10 и кол-во полей равно
        item.after($(newItem)); // добавляем новое поле
        fields++;
      }
    } else { // если имя файла пустое
      if (fields == fieldsAttached + 1) {
        item.remove(); // удаляем поле
        fields--;
      } else {
        item.replaceWith($(newItem)); // заменяем поле на "чистое"
      }
      fieldsAttached--;

      if (fields == 1) { // если поле осталось одно
        attach.find('.attach__up').text('Прикрепить документ с реквизитами'); // меняем текст
      }
    }
  });

  // При нажатии на "Изменить"
  attach.on('click', '.attach__edit', function() {
    $(this).closest('.attach__item').find('.attach__input').trigger('click'); // имитируем клик на инпут
  });

  // При нажатии на "Удалить"
  attach.on('click', '.attach__delete', function() {
    var item = $(this).closest('.' + fieldClass);
    if (fields > fieldsAttached) { // если полей больше, чем загруженных файлов
      item.remove(); // удаляем поле
      fields--;
    } else { // если равно
      item.after($(newItem)); // добавляем новое поле
      item.remove(); // удаляем старое
    }
    fieldsAttached--;
    if (fields == 1) { // если поле осталось одно
      attach.find('.attach__up').text('Прикрепить документ с реквизитами'); // меняем текст
    }
  });
});





var productSlider = $('.card-img__slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  dots: false,
  
  asNavFor: '.card-img__slider--nav'
});


$('.card-img__slider--nav').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  asNavFor: '.card-img__slider',
  dots: false,
  arrows: false,
  focusOnSelect: true,
  vertical: true,
  responsive: [
  {
    breakpoint: 1200,
    settings: {
      vertical: false,
    }
  },
  {
    breakpoint: 479,
    settings: {
      slidesToShow: 3,
      vertical: false,
    }
  },
  ]
});






// Меню статтей
$('.article-nav .drop > a').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('active');
  $(this).next('ul').toggleClass('active');
})




// Меню в футере на мобильном
if ($(window).width() < 992) {
  $('.footer-item__links .h3').click(function() {
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}











// Календарь
$(function () {
    $('#datetimepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});





 // Стилизация селектов
$('select').styler();




// Плейсфолдер в инпутах
const FloatLabel = (() => {
  
  // add active class and placeholder 
  const handleFocus = (e) => {
    const target = e.target;
    target.parentNode.classList.add('active');
    
  };
  
  // remove active class and placeholder
  const handleBlur = (e) => {
    const target = e.target;
    if(!target.value) {
      target.parentNode.classList.remove('active');
    }
     
  };  
  
  // register events
  const bindEvents = (element) => {
    const floatField = element.querySelector('input,textarea');
    floatField.addEventListener('focus', handleFocus);
    floatField.addEventListener('blur', handleBlur);    
  };
  
  // get DOM elements
  const init = () => {
    const floatContainers = document.querySelectorAll('.form-item');
    
    floatContainers.forEach((element) => {
      if (element.querySelector('input,textarea').value) {
          element.classList.add('active');
      }      
      
      bindEvents(element);
    });
  };
  
  return {
    init: init
  };
})();

FloatLabel.init();




// Показать/скрыть корзину при наведении

$('.header-cart').hover(function() {
  $(this).addClass('active');
  $('.header-cart__block').stop(true, true).fadeIn();
}, function(){
  $(this).removeClass('active');
  $('.header-cart__block').stop(true, true).fadeOut();
})



// меню каталога
$('.catalog-menu .drop').hover(function() {
  $('.catalog-menu .drop').removeClass('active');
  $(this).stop(true, true).toggleClass('active');
})




// FansyBox
 $('.fancybox').fancybox({
  closeExisting: true,
 });


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");

// Маска кода
$(".code-mask").mask("9 9 9 9");






// Календарь
$(function () {
    $('#datepicker').datetimepicker({
        locale: 'ru',
        format: 'DD/MM/YYYY'

    });
});























// Меню в футере на мобильном
if ($(window).width() <= 768) {
  $('.footer-links__item .h5').click(function() {
    console.log('click')
    $(this).next('ul').fadeToggle();
    $(this).toggleClass('active');
  })

}



// Переключатель
$('.switch-btn').click(function (e, changeState) {
    if (changeState === undefined) {
        $(this).toggleClass('switch-on');
    }
    if ($(this).hasClass('switch-on')) {
        $(this).trigger('on.switch');
    } else {
        $(this).trigger('off.switch');
    }
});



// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 100,
  max: 10000,
  values: [ 2000, 5600 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range




// Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrap').find('#' + _id);
    $(this).parents('.tabs-nav').find('a').removeClass('active');
    $(this).addClass('active');
     $(this).parents('.tabs-wrap').find('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});







// MMENU
if ($(window).width() < 1200) {
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});


if ($('body').hasClass('inner')) {
  var positionMenu = 'right'
}else{
  positionMenu = 'left'
}

var drawer = menu.offcanvas({
  position: positionMenu,
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});


}




$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});





$('.select-list ul li').click(function() {
  $('.select-list ul li').removeClass('active');
  $(this).addClass('active');
})



// Выбрать все в избранном
$("#favorites-select-all").click(function () {
     $('.favorites-item__check input:checkbox').not(this).prop('checked', this.checked);
 });














})